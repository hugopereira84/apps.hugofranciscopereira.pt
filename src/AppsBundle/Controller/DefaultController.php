<?php

namespace AppsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Goutte\Client;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    /**
     * WOrking
     */
    public function getGists(){
        $client = new Client();
        $client->request('GET', 'https://api.github.com/gists?access_token=885ac0b4719d0d92845f4fbf0c2792de89a920ae');
        $response = $client->getResponse();
        $list_gist = json_decode($response->getContent(), true);
    }

    protected function isValidFacebookAccount($id, $accessToken)
    {
        $client = new Client();
        $client->request('GET', sprintf('https://graph.facebook.com/me?access_token=%s', $accessToken));
        $response = json_decode($client->getResponse()->getContent());
        if ($response->error) {
            throw new InvalidPropertyUserException($response->error->message);
        }
        return $response->id == $id;
    }

    public function queryFacebookApi(Request $request){
        $accessTokenUrl = 'https://graph.facebook.com/v2.8/oauth/access_token';
        $graphApiUrl = 'https://graph.facebook.com/v2.8/me';
        $params = [
            'code' => $request->get('code'),
            'client_id' => '1683898385174777',
            'client_secret' => '80ea419fca5eb975b0ec8450b67ac440',
            'redirect_uri'  => 'http://apps.hugofranciscopereira.local/app_dev.php',

        ];

        $client = new \GuzzleHttp\Client();
        $accessToken = $client->get($accessTokenUrl, ['query' => $params]);
        $testvar = json_decode($accessToken->getBody()->getContents(), true);
        $testvar['fields'] = "id,name,work,picture.width(200).height(200),email";
        $profile = json_decode($client->get($graphApiUrl, ['query' => $testvar])->getBody()->getContents(), true);
        var_dump($profile);
    }

    public function indexAction(Request $request)
    {
        /*******************/
        $jsonTaskConfig = '{
            "type": "service_account",
            "project_id": "apps-hugofranciscopereira",
            "private_key_id": "0a90404427f1f9c1c0d44b61ea9c75d1a7ef975e",
            "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCv2d+/vlbIZU90\noLzGT/ARHuFheXUBDQMMoNfJMszCJxxnW8lmQgwpDMncPCLGGZXDgxfwigsZqnfl\nqetLsre4Ar5J4L0Uq5Y2+kTwYdqy4HXYcWpNP2zgm158dJvABRQIsRYpejTwrbfN\nP9mNcY1PE9DaMU1Eufy7LMXVGH41AW21B3dPGVOhFkfiO8dbHa7yfe6nxHYrV/ua\nPFHpAxMticsepooA4W6wCA5vw3FQkqIrIK6xWUIyrd1azF9H6H+wWVjEu+9i0kox\nNu8ldQHAFzZ4EXNUGARFvz7vlOK8YiY4n1GUtwRfli20xjL2XkfF71di7saWeL5c\n04c4B3hnAgMBAAECggEAZQS0FGIzWEtaqZTs3L9vGWaz8lrjbAcdiVOcomgcWCw/\nlOCVgbjPY4WFMI8bKtHnxri4N+YVGlljJvvFv2abQ4nP5oDEHeu0D0EV+UE9cSSg\njgYZGucklKKsVLJoBpbkO1n/cTRS1S/2NmJHh4mDs7mecLmBucld9ehyfUfkuG11\nqLHCGq16QLYCogeOK7In88I5A0L9Py3azpOPkBu3YzPxWutCKGaT3k0J8iqQxz2H\nNRajyQQgBnqxCmIMJGJtq6nByih4S9slT4RyXw8MNLvn6AyFxxVutXQMaSv3hnh9\nP0+YUhkeGRb8OEmsGhyTg9wDxaf3ftFzEqD1MHoRAQKBgQDdg+XQzUgLbl6LHiHm\n6+OAWhcPE55aRIvRHzSdKMorxEzDpyErOoGeCnZwdfcyxJFGqPce8ygmW5RWZcaM\ntHTHLt2XL14BDdgT5IOLwUcjAaj3J6G39dqp+dFodJxIa9xQganurSMELDCTBpow\nZIVUJyq/Ej9XFfpNrOpJMMHk3wKBgQDLOhx2qRiemHD/E+OV4uBmQmH/2q07VEfo\nypJvbAuX4ztdwDh4bRniPR4Er5FBlLh5TRV7gkyuiSDD9Tgmvcita+QcL1qmfw78\nBUNsSfMK/6djhE15vSS5o0ZG/uKe+euI9aWx4UITOawtc+zgtUGheavYml66MxQG\nCcAQUuQVeQKBgAIEkpJdXFumTLwtsRdebLsdlvovLpEZ8MAd7afE2FqftidOHXI8\nXTPoWn3ZxT+0JLKlFWM1ydbXnLlQwgdEGZA68A/RgaS20JIV0dDzGMfL2xMtTD7V\nwYEs4mh2L3pFaUNeewLOlvQRxvt/4uWv9Lr4hF4rN2J+s8IDYjr6c7m3AoGAYYDQ\n9TFt6XF0DT2qnFgMl6NTx2BQar2l/o9qMGEjMs1Hx1QZA9Y8a+osw8zEITXMSA5u\ntyItzjNmb/3v4c5+QI4aFFe341fkhY+06HiWTfmYgsuQLE9OFm66ogdaDTIVClHp\nHG3ZuMWb0PbkeG8ePO3WQAbYWfd1rOvypSJP4ekCgYEAuam8tCK8SP0ICbljMJK8\nJB0oj90A2DpYTvhPwsaZlktmXR7M+frHizIZqInETNj3orf0YammElffpGGoFSAe\nsAD3nz6JkMN3jVvGdREAwHzbSZ1lQU+2TLDGVbBLDqqP2LHvMOQprRlb7YOz1w8Y\n1dc/UYDbKlxyNoUX2VrubTQ=\n-----END PRIVATE KEY-----\n",
            "client_email": "hugopereira@apps-hugofranciscopereira.iam.gserviceaccount.com",
            "client_id": "110261315689450990966",
            "auth_uri": "https://accounts.google.com/o/oauth2/auth",
            "token_uri": "https://accounts.google.com/o/oauth2/token",
            "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
            "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/hugopereira%40apps-hugofranciscopereira.iam.gserviceaccount.com"
        }';
        $accessToken = json_decode($jsonTaskConfig, true);

        /**********************/

        $cookie_file = '/tmp/' . uniqid() . 'cookie';
        $app_id = '1683898385174777';
        $agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36';
        $agentWorking = 'Opera/9.80 (Series 60; Opera Mini/6.5.27309/34.1445; U; en) Presto/2.8.119 Version/11.10';
        $redirect_uri = 'http://apps.hugofranciscopereira.local/app_dev.php';
        $cookie = new \GuzzleHttp\Cookie\SessionCookieJar('SESSION_STORAGE');
        $jar = new \GuzzleHttp\Cookie\CookieJar();
        $cookieJar = new \GuzzleHttp\Cookie\FileCookieJar($cookie_file, TRUE);


        $client = new Client([
            'allow_redirects' => true,
            'debug' => true,
            'verify' => false,
            'curl' => array(
                CURLOPT_COOKIEFILE => $cookie_file,
                CURLOPT_COOKIEJAR => $cookie_file,
                CURLOPT_RETURNTRANSFER  => 1
            ),
            'cookies' => true,
            'defaults' => [
                'config' => [
                    'curl' => [
                        CURLOPT_HTTPHEADER     => array('Accept-Language: en'),

                        CURLOPT_COOKIE => true,
                        CURLOPT_COOKIESESSION => true,
                        CURLOPT_COOKIEJAR => $cookie,
                        CURLOPT_COOKIEFILE => $cookie,
                        CURLOPT_SSL_VERIFYPEER => false,
                        CURLOPT_SSL_VERIFYHOST => 2,
                        CURLOPT_REFERER => "https://www.facebook.com",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_USERAGENT => $agent
                    ]
                ]
            ]
        ]);

        $client->getCookieJar()->set($cookieJar);


        $crawler = $client->request(
            'GET',
            'https://www.facebook.com/v2.8/dialog/oauth?client_id='.$app_id.'&redirect_uri='.$redirect_uri,
            [
                'cookies' => $cookieJar,
                'debug' => true,
                'headers' => [
                    'User-Agent' => $agent,
                    'cookies' => $cookieJar,
                    'Accept-Language' => 'en'
                ]
            ]
        );

        $form = $crawler->filter('#login_form')->form();
        //$form = $crawler->selectButton('Iniciar Sessão')->form();

        $crawler = $client->submit($form, array(
            'email' => 'hugopereira84@gmail.com',
            'pass' => 'pvsnqv15973'
        ));

        echo '<pre>';
        var_dump($client);
        die();
        exit();
        var_dump($crawler->filter('[@id="elementId"]'));
        var_dump();
        die();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://www.facebook.com/v2.8/dialog/oauth?client_id=1683898385174777&redirect_uri=http://apps.hugofranciscopereira.local/app_dev.php');
        //curl_setopt($ch, CURLOPT_POSTFIELDS,'email='.urlencode('hugopereira84@gmail.com').'&pass='.urlencode('pvsnqv15973').'&login=Login');
        //curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT,'Opera/9.80 (Series 60; Opera Mini/6.5.27309/34.1445; U; en) Presto/2.8.119 Version/11.10');
        curl_setopt($ch, CURLOPT_REFERER,"https://www.facebook.com");
        $body = curl_exec($ch);
        var_dump($body);


        die();
        $redirect_uri = 'http://apps.hugofranciscopereira.local/app_dev.php';
        $app_id = '1683898385174777';
        $cookie_file = '/tmp/' . uniqid() . 'cookie';
        $cookieJar = new \GuzzleHttp\Cookie\SessionCookieJar('SESSION_STORAGE', true);

        echo 'https://www.facebook.com/v2.8/dialog/oauth?client_id='.$app_id.'&redirect_uri='.$redirect_uri;
       // die();
        $client = new Client();
        $client->request(
            'POST',
            'https://www.facebook.com/v2.8/dialog/oauth?client_id='.$app_id.'&redirect_uri='.$redirect_uri,
            [
                'cookies' => $cookieJar,
                'debug' => true,
                'headers' => [
                    'User-Agent' => $agentWorking,
                    'Accept'     => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
                ],
                'form_params' => [
                    'email' => 'hugopereira84@gmail.com',
                    'pass' => 'pvsnqv15973'
                ]
            ]
        );
        $response = $client->getResponse();
        echo '<pre>';
        var_dump($response);
        die();

        /********************************************************/

        /*$cookieJar = new \GuzzleHttp\Cookie\SessionCookieJar('SESSION_STORAGE', true);
        $client = new \GuzzleHttp\Client([
            'headers' => [
                'Referer' => 'https://www.facebook.com',
                'User-Agent' => 'Opera/9.80 (Series 60; Opera Mini/6.5.27309/34.1445; U; en) Presto/2.8.119 Version/11.10',
            ],
            'cookies' => $cookieJar
        ]);
        $accessToken = $client->post('https://m.facebook.com/login.php', [
                'config' => [
                    'curl' => [
                        CURLOPT_POST => 1,
                        CURLOPT_HEADER => 0,
                        CURLOPT_FOLLOWLOCATION => 1,
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_USERAGENT => 'Opera/9.80 (Series 60; Opera Mini/6.5.27309/34.1445; U; en) Presto/2.8.119 Version/11.10',
                        CURLOPT_REFERER => 'https://www.facebook.com'
                    ]
                ],
                'body' => json_encode(
                    [
                        'email' => urlencode('hugopereira84@gmail.com'),
                        'pass' => urlencode('pvsnqv15973')
                    ]
                )
            ]
        );
        echo '<pre>';
        var_dump($accessToken->getBody()->getContents());
        die();*/

        /********************************************************/

        $cookie_file = '/tmp/' . uniqid() . 'cookie';
        $agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36';
        $agentWorking = 'Opera/9.80 (Series 60; Opera Mini/6.5.27309/34.1445; U; en) Presto/2.8.119 Version/11.10';
        $cookieJar = new \GuzzleHttp\Cookie\SessionCookieJar('SESSION_STORAGE', true);

        $client = new Client();
        $client->request(
            'POST',
            'https://m.facebook.com/login.php',
            [
                'cookies' => $cookieJar,
                'debug' => true,
                'headers' => [
                    'User-Agent' => $agentWorking,
                    'Accept'     => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
                ],
                'form_params' => [
                    'email' => 'hugopereira84@gmail.com',
                    'pass' => 'pvsnqv15973'
                ]
            ]
        );
        $response = $client->getResponse();
        echo '<pre>';
        var_dump($response);
        die();

        die();
        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://m.facebook.com/login.php');
        curl_setopt($ch, CURLOPT_POSTFIELDS,'email='.urlencode('hugopereira84@gmail.com').'&pass='.urlencode('pvsnqv15973').'&login=Login');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT,'Opera/9.80 (Series 60; Opera Mini/6.5.27309/34.1445; U; en) Presto/2.8.119 Version/11.10');
        curl_setopt($ch, CURLOPT_REFERER,"https://www.facebook.com");
        $body = curl_exec($ch);
        var_dump($body);
        die();*/

        /*******************************************************************************/



        $client = new Client();
        $client->request(
            'GET',
            'https://www.facebook.com/v2.8/dialog/oauth?client_id=1683898385174777&redirect_uri=http://apps.hugofranciscopereira.local/app_dev.php',
            [
                'allow_redirects' => [
                    'max'             => 10,        // allow at most 10 redirects.
                    'strict'          => true,      // use "strict" RFC compliant redirects.
                    'referer'         => true,      // add a Referer header
                    'track_redirects' => true
                ],
                'cookies' => $cookieJar,
                'debug' => true,
                'headers' => [
                    'User-Agent' => $agentWorking,
                    'Accept'     => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
                ],
                'form_params' => [
                    'email' => 'hugopereira84@gmail.com',
                    'pass' => 'pvsnqv15973'
                ]
            ]
        );
        $response = $client->getResponse();
        echo '<pre>';
        var_dump($response);
        die();

        //$client->getClient()->setDefaultOption('config/curl/'.CURLOPT_COOKIEJAR, $cookie_file);
        /*$client->getClient()->setDefaultOption('config/curl/'.CURLOPT_SSL_VERIFYHOST, FALSE);
        $client->getClient()->setDefaultOption('config/curl/'.CURLOPT_VERBOSE, TRUE);

        $client->getClient()->setDefaultOption('config/curl/'.CURLOPT_SSL_VERIFYPEER, FALSE);
        $client->getClient()->setDefaultOption('config/curl/'.CURLOPT_RETURNTRANSFER, TRUE);
        $client->getClient()->setDefaultOption('config/curl/'.CURLOPT_USERAGENT, $agent);

        $client->getClient()->setDefaultOption('config/curl/'.CURLOPT_FOLLOWLOCATION, TRUE);

        $client->getClient()->setDefaultOption('config/curl/'.CURLOPT_COOKIESESSION, TRUE);
        $client->getClient()->setDefaultOption('config/curl/'.CURLOPT_COOKIEFILE, $cookie_file);



        $client->getClient()->setDefaultOption('config/curl/'.CURLOPT_TIMEOUT, 60);*/
        //$client->setHeader('User-Agent', "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36");
        //$client->setHeader('User-Agent', $agent2);

        /*$client->request('GET',
            'https://www.facebook.com/v2.8/dialog/oauth?client_id=1683898385174777&redirect_uri=http://apps.hugofranciscopereira.local/app_dev.php',
            [
                'allow_redirects' => [
                    'max'             => 10,        // allow at most 10 redirects.
                    'strict'          => true,      // use "strict" RFC compliant redirects.
                    'referer'         => true,      // add a Referer header
                    'protocols'       => ['https'], // only allow https URLs
                    'track_redirects' => true
                ],
                'cookies' => $jar,
                'debug' => true
            ]
        );

        $response = $client->getResponse();
        echo '<pre>';
        var_dump($response);
        die();
        $response = $client->getResponse();
        $list_labels = json_decode($response->getContent(), true) ;

        if(!$request->query->get('code')){

        }else{
            echo 'aa';
        }






        /*$client = new Client();
        $client->request('GET', 'https://api.github.com/repos/hugopereira84/tags/tags?access_token=885ac0b4719d0d92845f4fbf0c2792de89a920ae');
        $response = $client->getResponse();
        $list_labels = json_decode($response->getContent(), true) ;

        var_dump($list_labels);
        die();
        echo 'asdasd'; die();
        return $this->render('AppsBundle:Default:index.html.twig');*/
    }

    function fbAction(){
        echo 'fb'; die();
    }

    function ghAction(){
        echo 'Gh'; die();
    }
}
