<?php

namespace FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Goutte\Client;

class FrontendController extends Controller
{
    private function getUrlMapGinasioPN(){
        $client = new Client();
        $crawler = $client->request('GET', 'http://pump-spirit.com/');
        $linkClubes = $crawler->selectLink('Clubes')->link()->getUri();


        $crawler = $client->request('GET', $linkClubes);
        $resultCrawler = $crawler->filter(".treecolumnsgrid__item")->each(function ($node) {
            $textGinasio = trim($node->filter('.treecolumnsgrid__title')->text());
            if($textGinasio == "Pump Parque das Nações"){
                $linkGinasioPump = $node->filter('a')->link()->getUri();
                return [
                    'text' => $textGinasio,
                    'link'  => $linkGinasioPump
                ];
            }else{
                return [];
            }
        });
        $infoGinasioPN = array_values(array_filter($resultCrawler))[0];


        $crawler = $client->request('GET', $infoGinasioPN['link']);
        return $crawler->selectLink('Download mapa de aulas')->link()->getUri();
    }

    private function getCategoriesRecipes(){
        $client = new Client();
        $crawler = $client->request('GET', 'http://www.bewellwitharielle.com/blog');

        $result = [];
        $listCategories = $crawler->filter('.widget_categories')->filter('a')->links();
        foreach ($listCategories as $listCategory){
            $urlArray = parse_url($listCategory->getUri());
            $nameCategory = str_replace(['/', 'category'], '',$urlArray['path']);

            $result[] = [
                        'name' => $listCategory->getNode()->textContent,
                        'link'=> $listCategory->getUri(),
                        'nameCategory' => $nameCategory
                    ];
        }

        return $result;
    }
    private function getCategoriesRecipesList($nameCategory){
        $client = new Client();
        $crawler = $client->request('GET', 'http://www.bewellwitharielle.com/category/'.$nameCategory);

        $result = [];
        $listCategories = $crawler->filter('.post_box')->filter('a')->links();
        foreach ($listCategories as $listCategory){
            $urlArray = parse_url($listCategory->getUri());
            $nameCategory = str_replace(['/', 'category'], '',$urlArray['path']);

            $result[] = [
                'name' => $listCategory->getNode()->textContent,
                'link'=> $listCategory->getUri(),
                'nameCategory' => $nameCategory
            ];
        }

        echo '<pre>';
        var_dump($result);
        die();

        return $result;
    }

    /**
     * @Template("FrontendBundle:pages:index.html.twig")
     */
    public function indexAction()
    {
        return [
                'linkHorarioGinasioPump'=> $this->getUrlMapGinasioPN(),
                'categoriesRecipes' => $this->getCategoriesRecipes()
            ];
    }

    public function listCategoryRecipeAction($name){
        $this->getCategoriesRecipesList($name);
    }
}
